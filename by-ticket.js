const TelegrafInlineMenu = require('telegraf-inline-menu');

class ByTicket {
    constructor(lotteryArr, lotteryId, numbers) {
        this.lotteryArr = lotteryArr;
        this.lotteryId = lotteryId;
        this.numbers = numbers;
    }

    actionChose(lotteryName, lotteryId) {
        const buy = new TelegrafInlineMenu('Ваш результат "123" - тестові дані');

        // buy.simpleButton('test', `by1_${lotteryId}`, {doFunc: ctx => ctx.reply('Ticket purchased!')});

        buy.button('Купити', `buy_ticket_${lotteryId}`, {doFunc: ctx => ctx.reply('Ticket purchased!')})
        buy.button('Завантажити', `save_ticket_${lotteryId}`, {doFunc: ctx => ctx.reply('Ticket download!')})
        buy.button('Відправити на email', `send_ticket_to_email_${lotteryId}`, {doFunc: ctx => ctx.reply('Ticket send!')});

        let arrResult = [];

        const manualSelect = new TelegrafInlineMenu(
            ctx => 'Ваші номери: ' + arrResult.join(' ').replace(/\+/gi, ''))
            .select('f', this.numbers, {
                setFunc: (ctx, ch) => {
                    const ind = ctx.match[1];
                    let arr = this.numbers;

                    // console.log(typeof ind);

                    for (let i in arr) {
                        if (arr[i] === ind) {
                            if (ind.match(/\+/)) {
                                arr[i] = parseInt(ind.match(/\d+/)).toString();

                            } else {
                                arr[i] = '+' + ind;
                            }
                        }
                    }

                    arrResult = arr.filter(arr => arr.match(/\+/));
                    console.log(arrResult);
                },
            });

        const result = new TelegrafInlineMenu(`${lotteryName} Виберіть режим:`);

        if (lotteryName !== 'Лото забава') {
            result.submenu('Своя гра', `cc_${lotteryId}`, manualSelect);
        }

        result.submenu('Авто гра', `rc1_${lotteryId}`, buy);

        return result

    }

    replyTicket() {
        try {
            const subTicket = new TelegrafInlineMenu(ctx => `${ctx.from.first_name} Виберіть лотерею`);

            for (let name in this.lotteryArr) {
                subTicket.submenu(
                    `${this.lotteryArr[name]}`,
                    `b_${this.lotteryArr[name]}`,
                    this.actionChose(this.lotteryArr[name], this.lotteryId[name])
                );
            }

            return subTicket
        } catch (e) {
            console.log(e.message);
        }
    }

}

module.exports = ByTicket;