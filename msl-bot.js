const Telegraf = require('telegraf');
const Config = require('./config.json');
const Keyboard = require('telegraf-keyboard');
const TelegrafInlineMenu = require('telegraf-inline-menu');
const ByTicket = require('./by-ticket');


const session = require('telegraf/session');

console.log(Config.token);

const bot = new Telegraf(Config.token);

const lottery = ['Лото забава', 'Мегалот', 'Спортпрогноз'];

const draw = {
    draw_1: 'res 1',
    draw_2: 'res eam2',
    draw_3: 'res 3',
    draw_4: 'res 4',
    draw_5: 'res 5',
    draw_6: 'res 6',
};

const mainMenu = (new Keyboard())
    .add('Купити квиток')
    .add('Результати', 'Задати питання')
    .add('FAQ', 'Про нас')
    .add('Архів');

//===================RESULTS========================
function lotteryChose(lotteryName, results) {

    const result = new TelegrafInlineMenu(`Результати лотереї: ${lotteryName}`);
    result.selectSubmenu('lz',
        () => Object.keys(draw),
        new TelegrafInlineMenu((ctx) => {
            for (let results in draw) {
                if (ctx.match[1] === results) {
                    return `${draw[results]}`;
                }
            }

            throw 'Not found result';
        }),
        {
            columns: 2,
        }
    );

    return result
}


//============================================
function lotteryChoseForBuy (lotteryName, lotteries) {

    if(lotteries.isArray()){
        for(let name in lotteries) {
            if (lotteryName === name){
                return false
            }
        }
    }
}

const arr = ['1','2','3','4','5','6','7','8','9','0'];

const lottoResults = new TelegrafInlineMenu(ctx => `${ctx.from.first_name} виберіть лотерею`);

for (let name in lottery) {
    lottoResults.submenu(`${lottery[name]}`, `results_${lottery[name]}`, lotteryChose(lottery[name]));
}
//================================================

const archive = new TelegrafInlineMenu('list');
archive.submenu('list_1', 'l1', new TelegrafInlineMenu('info'));
const byTicket = new TelegrafInlineMenu(ctx => `${ctx.from.first_name} Вам є 18?`);

byTicket.submenu('Так', 'yes18', new ByTicket(lottery, Object.keys(lottery), arr).replyTicket());
byTicket.submenu('Ні', 'no18', new TelegrafInlineMenu('Тільки для повнолітніх'));

bot.hears('FAQ', ctx => ctx.reply(`FAQ FAQ FAQ\n FAQ FAQ FAQ\n FAQ FAQ FAQ\n FAQ FAQ FAQ\n FAQ FAQ FAQ\n`));

bot.hears('Про нас', ctx => ctx.reply(`Про нас Про нас Про нас\nПро нас Про нас Про нас\nПро нас Про нас Про нас\nПро нас Про нас Про нас\n`));

bot.hears('Задати питання', ctx => ctx.reply(`Про нас Про нас Про нас\nПро нас Про нас Про нас\nПро нас Про нас Про нас\nПро нас Про нас Про нас\n`));

bot.hears('Архів', archive.replyMenuMiddleware());


bot.use(archive.init({
    backButtonText: 'Назад',
    // mainMenuButtonText: 'back to main menu…'
}));

bot.hears('Результати', lottoResults.replyMenuMiddleware());
bot.use(lottoResults.init({
    backButtonText: 'Назад',
    // mainMenuButtonText: 'back to main menu…'
}));

bot.hears('Купити квиток', byTicket.replyMenuMiddleware());
bot.use(byTicket.init({
    backButtonText: 'Назад',
    // mainMenuButtonText: 'back to main menu…'
}));

bot.start((ctx) => {
    ctx.reply('Вітаємо... \n Джекпоти: -h-h-h-h-h-  \nМова: /ru (впроцесі)',
        mainMenu.draw());
})

    .hears('1', (ctx) => {
        ctx.reply('Шалом православние')
        // ctx.menu
    })
    .on('callback_query', (ctx) => {
        console.log(ctx.callbackQuery.data);
        ctx.answerCbQuery(ctx.callbackQuery.data)
    })
    .hears('Back', (ctx) => {
        // console.log(mainMenuKeyboard)
        ctx.reply('mine keyboard', mainMenu.draw())
    });


bot.launch();