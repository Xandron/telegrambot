const fetch = require('node-fetch');
const parser = require('body-parser');
const cheerio = require('cheerio');
const request = require('request');
const needle = require('needle');
const express = require('express');
const fs = require('fs');

const PuppeteerHandler = require('./helpers/puppeteer');

const baseUrl = 'https://igra.msl.ua/lotozabava/uk/archive';

// const sender = 'https://chat.sender.mobi/auth/back/https%3A%2F%2Fadmin.sender.mobi%2F';

// const startTime = new Date();

// request(baseUrl, (err, res, body) => {
//     if (err) throw err;
//
//     const $ = cheerio.load(body);
//
//     arr.push(
//         $('.lz-balls-container')
//             .text()
//             .replace(/\n/ig, '')
//             .replace(/ +/ig, ',')
//             .slice(1, -1));
// });

// console.log(arr);

// const q = tress((baseUrl, callback)=>{
//     needle.get(baseUrl, (err, res) => {
//         if (err) throw err;
//
//         const $ = cheerio.load(res.body);
//
//         resi.push(
//             $('.lz-balls-container')
//                 .text()
//                 .replace(/\n/ig, '')
//                 .replace(/ +/ig, ',')
//                 .slice(1, -1));
//         });
//
//     callback()
//
// }, 10);

let arr = [];
const baseDir = './resources';

function lotoZabavaResults(draw) {

    try {
        const url = `https://igra.msl.ua/lotozabava/uk/archive?draw=${draw}`;
        const currentDir = baseDir + '/loto_zabava_results';
        const file = currentDir + `/${draw}.json`;
        
        needle.get(url, (err, res) => {
            if (err) console.log(err);

            const $ = cheerio.load(res.body);

            const result = $('.lz-balls-container')
                .text()
                .replace(/\n/ig, '')
                .replace(/ +/ig, ',')
                .slice(1, -1);

            arr.push({
                'draw': draw,
                'date': 'current date',
                'balls': result,
                // 'couple': archiveParser(item, 'couple').then(result=>{return  result +1}),
                'winner sums': '',
            });

            if (!fs.existsSync(currentDir)) {
                fs.mkdirSync(currentDir);
            }

            if (!fs.existsSync(file)) {
                fs.writeFileSync(file, JSON.stringify(arr));
                return console.log('success')
            } else  {
                return console.log('Draw already exists!!!')
            }

        });

    } catch (e) {
        console.log(e.message)
    }

}

lotoZabavaResults(1064);

try {

    async function archiveParser(draw, type) {
        const url = `https://igra.msl.ua/lotozabava/uk/archive?draw=${draw}`;

        const pageContent = await (new PuppeteerHandler).getPageContent(url);
        const $ = cheerio.load(pageContent);


        if (type === 'balls') {
            return $('.lz-balls-container')
                .text()
                .replace(/\n/ig, '')
                .replace(/ +/ig, ',')
                .slice(1, -1);
        }

        if (type === 'couple') {
            return $('.lz-result-couple')
                .text()
                .replace(/\n/ig, '')
                .replace(/ +/ig, ',')
                .slice(1, -1);
        }

        return 1;
    }

    // console.log(archiveParser(1060, 'balls').then(res => res));

    async function main() {
        const pageContent = await (new PuppeteerHandler).getPageContent(baseUrl);
        const $ = cheerio.load(pageContent);

        const drawNumbers = $('.lz-archive-cell ')
            .children('.fc-dark')
            .text()
            .split('№')
            .map((arr, key) => arr.trim())
            .filter((arr, key) => {
                if (arr !== null && key <= 4)
                    return arr;
            });


        const drawDates = $('.lz-archive-cell')
            .text()
            .split('\n')
            .map(arr => arr.trim())
            .filter(arr => {
                if (arr.length === 10) {
                    return arr
                }
            });

        // console.log(drawDates);
        // .map((arr) =>{
        //     return arr.replace(/ \s+|\r?\n/g, " ").trim()
        // })
        // .filter((arr) => {if(arr !== null) return arr});
        // console.log(draws);
        // for (let key of draws.values){
        //     console.log(key)
        // }
        // console.log(draws)
        // const bigDraw = $('.')
        // const drawRes =  (new PuppeteerHandler).getPageContent(result + '?draw=1060');
        // const c2 = cheerio.load(drawRes);
        // console.log(c2('.lz-title').text());

        return drawNumbers.map((item, key) => {
            // console.log(arr);

            // console.log(archiveParser(item, 'balls').then(result => console.log(result)))
            return {
                'draw': item,
                'date': drawDates[key],
                // 'balls': archiveParser(item, 'balls').then(result => console.log(result)),
                // 'couple': archiveParser(item, 'couple').then(result=>{return  result +1}),
                'winner sums': '',
            }
        });

        // const drawNum = drawPrepare.split(' ')[6];
        // const n2 = drawPrepare;
        // // if( drawNum === n2){
        // //     console.log(123)
        // // }
        //
        //     let res = $('.lz-result-section').text();

    }

    // console.log(
    //     main()
    //         .then(res => console.log(res))
    //         // .catch(err => console.log(err))
    // );

} catch (e) {
    console.log(e.message);
}









