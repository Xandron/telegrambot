const Telegraf = require('telegraf')
const Composer = require('telegraf/composer')
const session = require('telegraf/session')
const Stage = require('telegraf/stage')
const Markup = require('telegraf/markup')
const WizardScene = require('telegraf/scenes/wizard')
const Config    =      require('./config.json');
const TelegrafInlineMenu = require('telegraf-inline-menu');
const fetch     =      require('node-fetch')
const Keyboard  =      require('telegraf-keyboard');

//==========================
const menu = new TelegrafInlineMenu(ctx => `Hey ${ctx.from.first_name}!`)

menu.setCommand('inline')

let mainMenuToggle = false
menu.toggle('toggle me', 'a', {
  setFunc: (_ctx, newVal) => {
    mainMenuToggle = newVal
  },
  isSetFunc: () => mainMenuToggle
})

menu.simpleButton('I am excited!', 'aasss', {
  doFunc: ctx => ctx.reply('As am I!')
})

menu.simpleButton('but 2', 'acss', {
  doFunc: async ctx => ctx.answerCbQuery('you clicked me!'),
  hide: () => false
})

//multi toggle
let selectedKey = 'badaboom'
menu.select('s', ['A', 'BOM', 'Com'], {
  setFunc: async (ctx, key) => {
    selectedKey = key
    if (selectedKey == 'A'){
      await ctx.reply(`you selected ${key}`)  
    }
    await ctx.answerCbQuery(`you selected ${key}`)
  },
  isSetFunc: (_ctx, key) => key === selectedKey
})







const stepHandler = new Composer()
stepHandler.action('inline', (ctx) => {
  ctx.reply('Step 2. Via inline button')
  return ctx.wizard.next()
})
stepHandler.command('next', (ctx) => {
  ctx.reply('Step 2. Via command')
  return ctx.wizard.next()
})
stepHandler.use((ctx) => ctx.replyWithMarkdown('Press `Next` button or type /next'))

const superWizard = new WizardScene('super-wizard',
  (ctx) => {
    ctx.reply('Step 1', Markup.inlineKeyboard([
      Markup.urlButton('❤️', 'http://telegraf.js.org'),
      Markup.callbackButton('inline', 'inline')
    ]).extra())
    return ctx.wizard.next()
  },
  stepHandler,
  (ctx) => {
    ctx.reply('Step 3')
    return ctx.wizard.next()
  },
  (ctx) => {
    ctx.reply('Step 4')
    return ctx.wizard.next()
  },
  (ctx) => {
    ctx.reply('Done')
    return ctx.scene.leave()
  }
)



const bot = new Telegraf(Config.token)
const stage = new Stage([superWizard], { default: 'super-wizard' })
bot.use(session())
bot.use(stage.middleware())
bot.hears('asd', (ctx) => {
  Markup.callbackButton('tp', 'pt')
})
bot.launch()
