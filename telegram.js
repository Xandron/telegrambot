const Telegraf  =      require('telegraf');
const Config    =      require('./config.json');
const Markup    =      require('telegraf/markup');
const fetch     =      require('node-fetch')
const Keyboard  =      require('telegraf-keyboard');
const TelegrafInlineMenu = require('telegraf-inline-menu');

const session = require('telegraf/session')

console.log(Config.token);

const bot = new Telegraf(Config.token); 


const menu = new TelegrafInlineMenu(ctx => `Hey ${ctx.from.first_name}!`)

menu.setCommand('inline')


let mainMenuToggle = false
menu.toggle('toggle me', 'a', {
  setFunc: (_ctx, newVal) => {
    mainMenuToggle = newVal
  },
  isSetFunc: () => mainMenuToggle
})

menu.simpleButton('I am excited!', 'aasss', {
  doFunc: ctx => ctx.reply('As am I!')
})

menu.simpleButton('but 2', 'acss', {
  doFunc: async ctx => ctx.answerCbQuery('you clicked me!'),
  hide: () => false
})

//multi toggle
let selectedKey = 'badaboom'
menu.select('s', ['A', 'BOM', 'Com'], {
  setFunc: async (ctx, key) => {
    selectedKey = key
    if (selectedKey == 'A'){
      await ctx.reply(`you selected ${key}`)  
    }
    await ctx.answerCbQuery(`you selected ${key}`)
  },
  isSetFunc: (_ctx, key) => key === selectedKey
})
//============



//food menu

const foodMenu = new TelegrafInlineMenu('People like food. What do they like?')

const people = {Mark: {}, Paul: {}}
const food = ['bread', 'cake', 'bananas']

function personButtonText(_ctx, key) {
  const entry = people[key]
  if (!entry || !entry.food) {
    return key
  }

  return `${key} (${entry.food})`
}


function foodSelectText(ctx) {
  const person = ctx.match[1]
  const hisChoice = people[person].food
  if (!hisChoice) {
    return `${person} is still unsure what to eat.`
  }

  return `${person} likes ${hisChoice} currently.`
}

const foodSelectSubmenu = new TelegrafInlineMenu(foodSelectText)
  .toggle('Prefer Tee', 't', {
    setFunc: (ctx, choice) => {
      const person = ctx.match[1]
      people[person].tee = choice
    },
    isSetFunc: ctx => {
      const person = ctx.match[1]
      return people[person].tee === true
    }
  })
  .select('f', food, {
    setFunc: (ctx, key) => {
      const person = ctx.match[1]
      people[person].food = key
    },
    isSetFunc: (ctx, key) => {
      const person = ctx.match[1]
      return people[person].food === key
    }
  })

foodMenu.selectSubmenu('p', () => Object.keys(people), foodSelectSubmenu, {
  textFunc: personButtonText,
  columns: 2
})

// root of evil - uniqueIdentifier
foodMenu.question('Add person', 'add', {
  questionText: 'Who likes food too?',
  setFunc: (_ctx, key) => {
    people[key] = {}
  },
  uniqueIdentifier: 'unique'
})

menu.submenu('Food menu', 'food', foodMenu, {
  hide: () => mainMenuToggle
})

//==========================

//changed photo in menu
let isAndroid = true
menu.submenu('Photo Menu', 'photo', new TelegrafInlineMenu('', {
  photo: () => isAndroid ? 'https://telegram.org/img/SiteAndroid.jpg' : 'https://telegram.org/img/SiteiOs.jpg'
}))
  .setCommand('photo')
  .simpleButton('Just a button', 'a', {
    doFunc: async ctx => ctx.answerCbQuery('Just a callback query answer')
  })
  .select('img', ['iOS', 'Android'], {
    isSetFunc: (_ctx, key) => key === 'Android' ? isAndroid : !isAndroid,
    setFunc: (_ctx, key) => {
      isAndroid = key === 'Android'
    }
  })
//===========================

// button `back` and `next`. Session???
bot.use(session())

bot.use((ctx, next) => {
  if (ctx.callbackQuery) {
    console.log('another callbackQuery happened', ctx.callbackQuery.data.length, ctx.callbackQuery.data)
  }

  return next()
})

bot.use(menu.init({
  backButtonText: 'back…',
  mainMenuButtonText: 'back to main menu…'
}))

bot.catch(error => {
  console.log('telegraf error', error.response, error.parameters, error.on || error)
})



bot.use(menu.init())
//start
// bot.start((ctx) => {
//     // console.log(ctx.update.message.from.id);
//     // const keyboard = new Keyboard({
//     //   inline: true,
//     //   newline: true,
//     // })
//     // keyboard.add(
//     //   'Line 1:show more more text',
//     //   'Line 2:my',
//     //   'Line 3:friend'
//     //   )

//     ctx.reply('Hi!',
//       Markup.inlineKeyboard([
//         Markup.callbackButton('text', 'my-callback-data')
//       ]))
//     // ctx.reply('send stiker or write "hi" or write "buttons"', Markup.inlineKeyboard([
//       // Markup.callbackButton('text', 'my-callback-data'))])
// 
//     })


//input text for action
bot.hears('hi', (ctx) => ctx.reply('Hey there'))
//===================

// console.log(ctx);
//example input @bot_name for get inline list
bot.on('inline_query', async ({ inlineQuery, answerInlineQuery }) => {
    const apiUrl = `http://recipepuppy.com/api/?q=${inlineQuery.query}`
    const response = await fetch(apiUrl)
    const { results } = await response.json()
    const recipes = results
      .filter(({ thumbnail }) => thumbnail)
      .map(({ title, href, thumbnail }) => ({
        type: 'article',
        id: thumbnail,
        title: title,
        description: title,
        thumb_url: thumbnail,
        input_message_content: {
          message_text: title
        },
        reply_markup: Markup.inlineKeyboard([
          Markup.urlButton('Go to recipe', href)
        ])
      }))
    return answerInlineQuery(recipes)
  })
  

 
//keyboard
let buttonOne = 'but 1';

const mainMenuKeyboard = (new Keyboard())
    .add('Show inline menu')
    .add('Main menu', 'Inline Menu')
    .add('Help')


bot.start( (ctx) => {
  ctx.reply('mine keyboard', mainMenuKeyboard.draw());
})
.hears('Main menu', ctx => {
  const keyboard = new Keyboard();
  keyboard.add('Optional');
  keyboard.add('Back');
  ctx.reply('Main menu', keyboard.draw())
})
.hears('Optional', (ctx) => {
  console.log(menu.init())
  
  
  // ctx.menu
})
.hears('Back', (ctx) => {
  // console.log(mainMenuKeyboard)
  ctx.reply('mine keyboard', mainMenuKeyboard.draw())
})
.hears(['Help', '42'], (ctx) => {
  mainMenuKeyboard.rename('Help', '42')
  ctx.reply('Answer to the Ultimate Question of Life, the Universe, and Everything', mainMenuKeyboard.draw())
})
.hears('Show inline menu', (ctx) => {
  
  const menu = new TelegrafInlineMenu(ctx => `Hey ${ctx.from.first_name}!`)

  let mainMenuToggle = false
  menu.toggle('toggle me', 'a', {
    setFunc: (_ctx, newVal) => {
      mainMenuToggle = newVal
    },
    isSetFunc: () => mainMenuToggle
  })
  
  menu.simpleButton('I am excited!', 'aasss', {
    doFunc: ctx => ctx.reply('As am I!')
  })
  
  menu.simpleButton('but 2', 'acss', {
    doFunc: async ctx => ctx.answerCbQuery('you clicked me!'),
    hide: () => false
  })
  console.log(menu)
  
})
.hears('Inline Menu', (ctx) => {
  const keyboard = new Keyboard({
      inline: true,
      newline: true,
  })
  keyboard.add('Line 1:hello', 'Line 2:my', 'Line 3:friend')
  ctx.reply(keyboard.draw())
})
.hears(buttonOne, ctx => ctx.reply('123'))
.on('callback_query', (ctx) => {
  ctx.answerCbQuery(ctx.callbackQuery.data)
})
// .hears('Show inline menu', ctx =>ctx.setCommand('start'))

bot.on('chosen_inline_result', ({ chosenInlineResult }) => {
    console.log('chosen inline result', chosenInlineResult)
})

// bot.use(menu.init(mainMenuKeyboard))

bot.launch()